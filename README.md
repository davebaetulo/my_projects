Welcome to the repository of my projects, I'm using this as a supplement to my resume. 

Here you may find the **debates**, **assistances** and **contributions** that I've been involved with the Data Science community:

# STACK EXCHANGE <br/>
User: https://stackexchange.com/users/11755257/dave?tab=accounts<br/>
**Cross Validated** : https://stats.stackexchange.com/users/260122/dave<br/>
**Data Science** : https://datascience.stackexchange.com/users/74599/dave<br/>
**Stack Overflow** : https://stackoverflow.com/users/8603149/dave<br/>

# KAGGLE <br/>
https://www.kaggle.com/baetulo
